package fombellida.angel.timer;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {

    private Button btnStart;
    private Button btnStop;
    private Button btnPause;
    private EditText inputInterval;
    private TextView textLaps;
    private TextView textTime;
    private Timer timer;
    private MediaPlayer mp;
    private double period;
    private int laps;
    private boolean running;
    private boolean started;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = findViewById(R.id.btnStart);
        btnStop = findViewById(R.id.btnStop);
        btnPause = findViewById(R.id.btnPause);
        textLaps = findViewById(R.id.textLaps);
        textTime = findViewById(R.id.textTime);
        inputInterval = findViewById(R.id.inputInterval);
        mp = MediaPlayer.create(getApplicationContext(), R.raw.tic);
        laps = 0;
        running = false;
        started = false;
        setListeners();
    }

    private void setListeners() {
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopTimer();
                laps = 0;
                updateLabels(laps, 0);

                if (inputInterval.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Default frequency is 1 second", Toast.LENGTH_SHORT).show();
                    period = 1.0;
                } else {
                    period = Double.parseDouble(inputInterval.getText().toString());
                    Toast.makeText(getApplicationContext(), "set timer with frequency of " + period + "  seconds", Toast.LENGTH_SHORT).show();
                }
                started = true;
                startTimer();
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                }
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (started && running) {
                    stopTimer();
                    btnPause.setText("RESUME");
                    btnPause.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.icons8_play_24), null, null, null);
                } else if (started) {
                    startTimer();
                    btnPause.setText("PAUSE");
                    btnPause.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.icons8_pausa_24), null, null, null);
                }
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopTimer();
                updateLabels(0, 0);
                started = false;
                btnPause.setText("PAUSE");
                btnPause.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.icons8_pausa_24), null, null, null);
            }
        });
    }

    private void startTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        running = true;
                        mp.start();
                        laps++;
                        updateLabels(laps, laps * period);
                    }
                });
            }
        }, 0, (long) (period * 1000));
    }

    private void stopTimer() {
        if (running) {
            timer.cancel();
            running = false;
        }
    }

    private void updateLabels(int laps, double seconds) {
        textLaps.setText(laps + " laps");
        textTime.setText(String.format("%.2f", seconds) + " seconds");
    }

}
