# Timer App
Very simple app. The user inserts a time period (in seconds) and the app will beep with that frequency (default is 1 second). It also keeps track of the laps and seconds passed since the start, and it is posible to pause/resume or stop the timer. Very useful to manage your training, cooking, or any activity that needs timing.

### Pre-requisitos
Android 7.0 (Nougat)